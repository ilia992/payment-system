package ru.rookonroad.service.payment.security;

import lombok.RequiredArgsConstructor;
import ru.rookonroad.service.payment.entity.Users;

@RequiredArgsConstructor
public final class JwtUserFactory {

    public static JwtUser create(Users user){
        return new JwtUser(
                user.getId(),
                user.getLogin(),
                user.getFirstName(),
                user.getLastName(),
                user.getUserName(),
                user.getPassword(),
                user.getUpdateTime(),
                user.getRegisterStatus(),
                user.getRole()
        );
    }
}
