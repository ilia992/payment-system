package ru.rookonroad.service.payment.security;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.rookonroad.service.payment.entity.Users;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

@RequiredArgsConstructor
@Getter
@Setter
public class JwtUser implements UserDetails {
    private final UUID id;
    private final String login;
    private final String firstName;
    private final String lastName;
    private final String userName;
    private final String password;
    private final LocalDateTime lastPasswordResetDate;
    private final Users.RegisterStatus registerStatus;
    private final Users.Role role;


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return new ArrayList<Users.Role>(){{add(role);}};
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }
}
