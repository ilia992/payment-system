package ru.rookonroad.service.payment.security;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import ru.rookonroad.service.payment.dto.PaymentProcessResponseDto;
import ru.rookonroad.service.payment.entity.System;
import ru.rookonroad.service.payment.model.PaymentCondition;
import ru.rookonroad.service.payment.repository.SystemRepository;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@RequiredArgsConstructor
public class SecretKeyFilter extends OncePerRequestFilter {
    SystemRepository systemRepository;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String secretKey = request.getHeader("SECRET-KEY");
        System systemTmp = systemRepository.findSystemBySecretKey(secretKey).orElse(null);
        if(systemTmp == null){
            response.sendError(403);
        }else {
            filterChain.doFilter(request, response);
        }
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        String path = request.getContextPath();
        return !path.equals("/payment/pay") && !path.equals("/payment/cancel");
    }
}
