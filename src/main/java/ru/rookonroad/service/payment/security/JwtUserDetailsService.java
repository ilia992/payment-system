package ru.rookonroad.service.payment.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.rookonroad.service.payment.dto.UsersResponseDto;
import ru.rookonroad.service.payment.entity.Users;
import ru.rookonroad.service.payment.repository.UsersRepository;
import ru.rookonroad.service.payment.service.AdminService;

import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class JwtUserDetailsService implements UserDetailsService {
    private final UsersRepository usersRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Users user = usersRepository.findByUserName(userName).orElse(null);
        if(user == null){
            throw new UsernameNotFoundException("User with username: " + userName + " not found");
        }
        JwtUser jwtUser = JwtUserFactory.create(user);
        log.info("IN loadUserByUsername - user with username: {} successful loaded", userName);
        return jwtUser;
    }
}
