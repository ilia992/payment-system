package ru.rookonroad.service.payment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.rookonroad.service.payment.entity.System;

import java.util.List;
import java.util.Optional;

@Repository
public interface SystemRepository extends JpaRepository<System, Long> {

    Optional<System>findSystemBySecretKey(String secretKey);

}
