package ru.rookonroad.service.payment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.rookonroad.service.payment.entity.Payment;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {

    List<Payment>findPaymentByTimestampAfter(LocalDateTime timestamp);

    List<Payment>findPaymentByTimestampBetween(LocalDateTime timestampStart, LocalDateTime timestampEnd);

}
