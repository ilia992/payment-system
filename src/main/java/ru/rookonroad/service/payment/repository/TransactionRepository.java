package ru.rookonroad.service.payment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.rookonroad.service.payment.entity.Transaction;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    List<Transaction> findTransactionByTimestampAfter(LocalDateTime timestamp);

    List<Transaction> findTransactionByTimestampBetween(LocalDateTime timeStart, LocalDateTime timeEnd);

    //Transaction findTransactionByPaymentAndAmount(Payment payment, BigDecimal amount);

    Optional<Transaction> findTransactionByIdAndStatus(Long transactionId, Transaction.Status status);

    List<Transaction> findAllByStatus(Transaction.Status status);
}
