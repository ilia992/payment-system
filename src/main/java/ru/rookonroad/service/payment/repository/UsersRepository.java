package ru.rookonroad.service.payment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.rookonroad.service.payment.entity.Users;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UsersRepository extends JpaRepository<Users, UUID> {

    List<Users>findAllByRole(Users.Role role);

    Optional<Users> findByLogin(String login);

    Optional<Users> findByUserName(String userName);

}
