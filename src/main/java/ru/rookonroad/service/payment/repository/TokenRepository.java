package ru.rookonroad.service.payment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.rookonroad.service.payment.entity.UserToken;

@Repository
public interface TokenRepository extends JpaRepository<UserToken, Long> {

}
