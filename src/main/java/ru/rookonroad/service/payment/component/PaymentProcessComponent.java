package ru.rookonroad.service.payment.component;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.rookonroad.service.payment.entity.Transaction;
import ru.rookonroad.service.payment.repository.TransactionRepository;
import ru.rookonroad.service.payment.service.NotificationService;

import javax.annotation.processing.ProcessingEnvironment;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import static java.lang.Math.random;

@Component
@RequiredArgsConstructor
public class PaymentProcessComponent {
    private final NotificationService notificationService;
    private final TransactionRepository transactionRepository;

    @Scheduled(fixedDelay = 1000)
    public void process(){
        List<Transaction> transactionList = transactionRepository.findAllByStatus(Transaction.Status.CREATED);
        transactionList.forEach(o -> o.setStatus(Transaction.Status.PROCESSING));

        transactionRepository.saveAll(transactionList);
    }
    @Scheduled(fixedDelay = 2000)
    public void payment(){
        List<Transaction> transactionListProcessing = transactionRepository.findAllByStatus(Transaction.Status.PROCESSING);
        Random random = new Random();
        transactionListProcessing.forEach( p -> {
            if((random.nextInt(100) % 2) == 0){
                p.setType(Transaction.Type.PAY);
                sleepBeforeNotification();
                successTransactionNotification();
            }else{
                p.setType(Transaction.Type.CANCEL);
                sleepBeforeNotification();
                failedTransactionNotification();
            }
        });
    }

    private void successTransactionNotification(){
        notificationService.sendSuccessfulTransaction();
    }

    private void failedTransactionNotification(){
        notificationService.sendFailedTransaction();
    }

    private void sleepBeforeNotification(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
