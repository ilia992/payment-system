package ru.rookonroad.service.payment.entity;

import lombok.*;
import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name = "user_token")
public class UserToken {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private Users userId;
    @Column(name = "refresh_token")
    private String refreshToken;
    @Column(name = "refresh_token_exp_date")
    private LocalDateTime refreshTokenExpiredData;

}
