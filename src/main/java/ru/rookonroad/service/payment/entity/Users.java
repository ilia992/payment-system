package ru.rookonroad.service.payment.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "users")
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Users {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "user_name")
    private String userName;
    @Enumerated(EnumType.STRING)
    private Role role;
    private String login;
    private String password;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "system_id")
    private System systemId;
    @Enumerated(EnumType.STRING)
    private RegisterStatus registerStatus;
    private LocalDateTime updateTime;

    public enum Role implements GrantedAuthority {
        ADMIN,
        EMPLOYEE,
        CLIENT;

        @Override
        public String getAuthority() {
            return this.name();
        }
    }
    public enum RegisterStatus{
        ACTIVE, BLOCKED
    }
}
