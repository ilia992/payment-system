package ru.rookonroad.service.payment.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class PaymentResponseDto {
    private Long id;
    private LocalDateTime timestamp;
    private BigDecimal amount;
    private Long systemId;
}
