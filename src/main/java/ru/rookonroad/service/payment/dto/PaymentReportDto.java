package ru.rookonroad.service.payment.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.rookonroad.service.payment.entity.Payment;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class PaymentReportDto {
    private List<PaymentResponseDto> paymentList;
}
