package ru.rookonroad.service.payment.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.rookonroad.service.payment.entity.Transaction;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class PaymentProcessDto {
    private Long transactionId;
    private BigDecimal amount;
    private Transaction.Status status;
    private String orderId;
}
