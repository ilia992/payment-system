package ru.rookonroad.service.payment.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.rookonroad.service.payment.entity.Transaction;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class TransactionResponseDto {
    private Long id;
    private LocalDateTime timestamp;
    private BigDecimal amount;
    private Transaction.Type type;
    private Transaction.Status status;
    private Long paymentId;
}
