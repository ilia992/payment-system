package ru.rookonroad.service.payment.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SystemResponseDto {
    private Long id;
    private String name;
}
