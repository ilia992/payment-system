package ru.rookonroad.service.payment.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.rookonroad.service.payment.entity.Users;

@Setter
@Getter
@NoArgsConstructor
public class UsersResponseDto {
//    private Long id;
    private String firstName;
    private String secondName;
    private System system;
    private Users.Role role;
}
