package ru.rookonroad.service.payment.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.rookonroad.service.payment.entity.Users;

@Getter
@Setter
@NoArgsConstructor
public class UsersRequestDto {
//    private Long id;
    private String firstName;
    private String lastName;
    private Users.Role role;
    private String login;
    private String password;
}
