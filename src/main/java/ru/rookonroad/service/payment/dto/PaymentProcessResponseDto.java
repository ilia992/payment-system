package ru.rookonroad.service.payment.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.rookonroad.service.payment.model.PaymentCondition;

@Getter
@Setter
@NoArgsConstructor
public class PaymentProcessResponseDto {
    private PaymentCondition paymentCondition;
    private long transactionId;

    public PaymentProcessResponseDto(PaymentCondition paymentCondition) {
        this.paymentCondition = paymentCondition;
    }

    @Override
    public String toString() {
        return paymentCondition.toString();
    }
}
