package ru.rookonroad.service.payment.mapper;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import ru.rookonroad.service.payment.dto.UsersRequestDto;
import ru.rookonroad.service.payment.dto.UsersResponseDto;
import ru.rookonroad.service.payment.entity.Users;

@Mapper(componentModel = "spring")
public interface UsersMapper {
    UsersResponseDto toDto(Users entity);
    Users toEntity(UsersRequestDto dto);
    void update(Users source, @MappingTarget Users target);
}