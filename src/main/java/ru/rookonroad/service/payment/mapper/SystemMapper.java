package ru.rookonroad.service.payment.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import ru.rookonroad.service.payment.dto.SystemRequestDto;
import ru.rookonroad.service.payment.dto.SystemResponseDto;
import ru.rookonroad.service.payment.entity.System;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SystemMapper {
    SystemResponseDto toDto(System entity);
    System toEntity(SystemRequestDto dto);
    void update(System source, @MappingTarget System target);
}
