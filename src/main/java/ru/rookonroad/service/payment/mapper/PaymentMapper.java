package ru.rookonroad.service.payment.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.rookonroad.service.payment.dto.PaymentResponseDto;
import ru.rookonroad.service.payment.entity.Payment;

@Mapper(componentModel = "spring")
public interface PaymentMapper {
    @Mapping(target = "systemId", source = "system.id")
    PaymentResponseDto toDto(Payment entity);
}
