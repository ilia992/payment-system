package ru.rookonroad.service.payment.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.rookonroad.service.payment.dto.SystemRequestDto;
import ru.rookonroad.service.payment.dto.SystemResponseDto;
import ru.rookonroad.service.payment.dto.TransactionResponseDto;
import ru.rookonroad.service.payment.entity.Transaction;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TransactionMapper {
    @Mapping(target = "paymentId", source = "payment.id")
    TransactionResponseDto toDto(Transaction entity);
}
