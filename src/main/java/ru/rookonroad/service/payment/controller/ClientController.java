package ru.rookonroad.service.payment.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.rookonroad.service.payment.dto.UsersRequestDto;
import ru.rookonroad.service.payment.dto.UsersResponseDto;

@RequiredArgsConstructor
@RestController
@RequestMapping("/client")
public class ClientController {
    @PutMapping("/refactor")
    public UsersResponseDto getClientById(@RequestBody UsersRequestDto usersRequestDto){
        return null;
    }

    @GetMapping("/delete-{id}")
    public UsersResponseDto deleteClient(@PathVariable Long id){
        return null;
    }


}
