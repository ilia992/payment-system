package ru.rookonroad.service.payment.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.rookonroad.service.payment.dto.UsersRequestDto;
import ru.rookonroad.service.payment.dto.UsersResponseDto;
import ru.rookonroad.service.payment.entity.Users;
import ru.rookonroad.service.payment.service.AdminService;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/admin")
@RequiredArgsConstructor
public class AdminController {
    private final AdminService adminService;

    @GetMapping("/{id}")
    public UsersResponseDto getUserById(@PathVariable UUID id){
        return adminService.getUserById(id);
    }

    @GetMapping("/{role}-all")
    public List<UsersResponseDto> getAllUsersByRole(@PathVariable Users.Role role){
        return adminService.getAllUsers(role);
    }
    @DeleteMapping("/{id}")
    public UsersResponseDto deleteUserById(@PathVariable UUID id){
        return adminService.deleteUserById(id);
    }
    @PostMapping("/")
    public UsersResponseDto registerUser(@RequestBody UsersRequestDto usersRequestDto){
        return adminService.registerUser(usersRequestDto);
    }
}
