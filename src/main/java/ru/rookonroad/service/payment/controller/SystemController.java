package ru.rookonroad.service.payment.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.rookonroad.service.payment.dto.SystemRequestDto;
import ru.rookonroad.service.payment.dto.SystemResponseDto;
import ru.rookonroad.service.payment.service.SystemService;

import java.util.List;

@RestController
@RequestMapping("/system")
@RequiredArgsConstructor
@Api(description = "System management ")
public class SystemController {
    private final SystemService systemService;

    @GetMapping("/all")
    @ApiOperation("Getting the list of all systems")
    public List<SystemResponseDto> getAllSystem() {
        return systemService.getAll();
    }

    @GetMapping("/{id}")
    @ApiOperation("Getting system by id")
    public SystemResponseDto getSystemById(@PathVariable Long id) {
        return systemService.getById(id);
    }

    @PostMapping("/create")
    @ApiOperation("Create a new system")
    public SystemResponseDto createSystem(@RequestBody SystemRequestDto dto) {
        return systemService.create(dto);
    }

    @PostMapping("/update/{id}")
    @ApiOperation("Update the system")
    public SystemResponseDto updateSystem(@PathVariable Long id, @RequestBody SystemRequestDto dto) {
        return systemService.update(id, dto);
    }
    @DeleteMapping("/{id}")
    @ApiOperation("Delete a system")
    public SystemResponseDto deleteSystem(@PathVariable Long id) {
        return systemService.delete(id);
    }
}
