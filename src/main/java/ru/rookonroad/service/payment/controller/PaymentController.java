package ru.rookonroad.service.payment.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.rookonroad.service.payment.dto.PaymentProcessDto;
import ru.rookonroad.service.payment.dto.PaymentProcessResponseDto;
import ru.rookonroad.service.payment.dto.PaymentResponseDto;
import ru.rookonroad.service.payment.service.PaymentProcessService;
import ru.rookonroad.service.payment.service.PaymentService;

import java.util.List;

@RestController
@RequestMapping("/payment")
@RequiredArgsConstructor
public class PaymentController {
    private final PaymentService paymentService;
    private final PaymentProcessService paymentProcessService;

    @GetMapping("/{id}")
    public PaymentResponseDto getPaymentById(@PathVariable Long id) {
        return paymentService.getById(id);
    }

    @GetMapping("/all")
    public List<PaymentResponseDto> getAllPayment() {
        return paymentService.getAll();
    }

    @DeleteMapping("/{id}")
    public PaymentResponseDto deletePayment(@PathVariable Long id) {
        return paymentService.delete(id);
    }

    @PostMapping("/pay")
    public PaymentProcessResponseDto pay(@RequestBody PaymentProcessDto paymentProcessDto){
        return paymentProcessService.pay(paymentProcessDto);
    }
    @PostMapping("/cancel")
    public PaymentProcessResponseDto cancel(@RequestBody PaymentProcessDto paymentProcessDto){
        return paymentProcessService.cancel(paymentProcessDto);
    }
}