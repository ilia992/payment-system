package ru.rookonroad.service.payment.controller;

import org.springframework.web.bind.annotation.*;
import ru.rookonroad.service.payment.dto.PaymentReportDto;
import ru.rookonroad.service.payment.dto.TransactionReportDto;
import ru.rookonroad.service.payment.service.ReportService;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/report")
public class ReportController {
    private final ReportService reportService;

    public ReportController(ReportService reportService) {
        this.reportService = reportService;
    }

    @GetMapping("/payment")
    public PaymentReportDto getPaymentTime(@RequestParam LocalDateTime time1, @RequestParam(required = false) LocalDateTime time2){
        if(time2 == null){
            return reportService.getPaymentAfterTime(time1);
        }
        return reportService.getPaymentBetweenTime(time1, time2);
    }

    @GetMapping("/transaction")
    public TransactionReportDto getTransactionTime(@RequestParam LocalDateTime time1, @RequestParam(required = false) LocalDateTime time2){
        if(time2 == null){
            return reportService.getTransactionAfterTime(time1);
        }
        return reportService.getTransactionBetweenTime(time1, time2);
    }
}
