package ru.rookonroad.service.payment.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.rookonroad.service.payment.dto.TransactionResponseDto;
import ru.rookonroad.service.payment.service.TransactionService;

import java.util.List;

@RestController
@RequestMapping("/transaction")
@RequiredArgsConstructor
public class TransactionController {
    private final TransactionService transactionService;

    @GetMapping("/{id}")
    public TransactionResponseDto getTransactionById(@PathVariable Long id) {
        return transactionService.getById(id);
    }

    @GetMapping("/get-all")
    public List<TransactionResponseDto> getAllTransaction() {
        return transactionService.getAll();
    }

    @DeleteMapping("/{id}")
    public TransactionResponseDto deleteTransaction(@PathVariable Long id) {
        return transactionService.delete(id);
    }
}
