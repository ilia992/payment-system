package ru.rookonroad.service.payment.model;

import lombok.Getter;
import lombok.Setter;

@Getter
public enum PaymentCondition {

    SUCCESSFUL ("success"),
    ERROR ("problems"),
    CREATED("created");

    private final String condition;

    PaymentCondition(String s) {
        this.condition = s;
    }

    @Override
    public String toString() {
        return condition;
    }
}