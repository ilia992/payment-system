package ru.rookonroad.service.payment.service;

import ru.rookonroad.service.payment.dto.PaymentReportDto;
import ru.rookonroad.service.payment.dto.TransactionReportDto;

import java.time.LocalDateTime;

public interface ReportService {
    PaymentReportDto getPaymentAfterTime(LocalDateTime timestamp);

    PaymentReportDto getPaymentBetweenTime(LocalDateTime timeStart, LocalDateTime timeEnd);

    TransactionReportDto getTransactionAfterTime(LocalDateTime timestamp);

    TransactionReportDto getTransactionBetweenTime(LocalDateTime timeStart, LocalDateTime timeEnd);

}
