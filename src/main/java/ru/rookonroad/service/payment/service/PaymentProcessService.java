package ru.rookonroad.service.payment.service;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestHeader;
import ru.rookonroad.service.payment.dto.PaymentProcessDto;
import ru.rookonroad.service.payment.dto.PaymentProcessResponseDto;


public interface PaymentProcessService {
    PaymentProcessResponseDto pay(PaymentProcessDto paymentProcessDto);
    PaymentProcessResponseDto cancel (PaymentProcessDto paymentProcessDto);

}
