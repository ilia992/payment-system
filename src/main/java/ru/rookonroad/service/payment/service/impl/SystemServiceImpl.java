package ru.rookonroad.service.payment.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.rookonroad.service.payment.dto.SystemRequestDto;
import ru.rookonroad.service.payment.dto.SystemResponseDto;
import ru.rookonroad.service.payment.mapper.SystemMapper;
import ru.rookonroad.service.payment.repository.SystemRepository;
import ru.rookonroad.service.payment.service.SystemService;
import ru.rookonroad.service.payment.entity.System;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;


@Service
@Slf4j
@RequiredArgsConstructor
public class SystemServiceImpl implements SystemService {
    private final SystemRepository systemRepository;
    private final SystemMapper systemMapper;

    @Override
    public SystemResponseDto getById(Long id) {
        System system = systemRepository.findById(id).orElse(null);
        if(system == null){
            log.warn("IN getById (system) - System with id: {} not found", id);
        }
        log.info("IN getById (system) - the System with id {} was sent to Dto", id);
        return systemMapper.toDto(system);
    }

    @Override
    public SystemResponseDto create(SystemRequestDto systemRequestDto) {
        System system = systemMapper.toEntity(systemRequestDto);
        system.setModifiedAt(LocalDateTime.now());
        system.setCreatedAt(LocalDateTime.now());
        log.info("IN create (system) - the System with id {} was create", systemRequestDto.getId());
        return systemMapper.toDto(systemRepository.save(system));
    }

    @Override
    public SystemResponseDto update(Long id, SystemRequestDto update) {
        System system = systemRepository.findById(id).orElse(null);
        if(system == null){
            log.warn("IN getById (system) - System with id: {} not found", id);
        }
        systemMapper.update(systemMapper.toEntity(update), system);
        assert system != null;
        system.setModifiedAt(LocalDateTime.now());
        log.info("IN update (system) - the System with id {} was updated", id);
        return systemMapper.toDto(systemRepository.save(system));
    }

    @Override
    public SystemResponseDto delete(Long id) {
        System system = systemRepository.findById(id).orElse(null);
        if(system == null){
            log.warn("IN getById (system) - System with id: {} not found", id);
        }
            assert system != null;
            systemRepository.delete(system);
            log.info("IN delete (system) - the System with id {} was deleted", id);
        return systemMapper.toDto(system);
    }

    @Override
    public List<SystemResponseDto> getAll() {
        List<System> list = systemRepository.findAll();
        if(list.isEmpty()){
            log.warn("IN getAll(system) - List of Systems is Empty");
        }
        log.info("IN getAll(system) - all Systems was sent to Dto");
        return list.stream().map(systemMapper::toDto).collect(Collectors.toList());
    }
}
