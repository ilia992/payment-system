package ru.rookonroad.service.payment.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.rookonroad.service.payment.dto.PaymentProcessDto;
import ru.rookonroad.service.payment.dto.PaymentProcessResponseDto;
import ru.rookonroad.service.payment.entity.Payment;
import ru.rookonroad.service.payment.entity.Transaction;
import ru.rookonroad.service.payment.model.PaymentCondition;
import ru.rookonroad.service.payment.repository.PaymentRepository;
import ru.rookonroad.service.payment.repository.TransactionRepository;
import ru.rookonroad.service.payment.service.PaymentProcessService;
import javax.transaction.Transactional;
import java.time.LocalDateTime;


@Service
@Slf4j
@RequiredArgsConstructor
public class PaymentProcessServiceImpl implements PaymentProcessService {
    private final PaymentRepository paymentRepository;
    private final TransactionRepository transactionRepository;

    @Override
    @Transactional
    public PaymentProcessResponseDto pay(PaymentProcessDto paymentProcessDto) {
        Payment payment = new Payment();
        payment.setAmount(paymentProcessDto.getAmount());
        payment.setTimestamp(LocalDateTime.now());
        paymentRepository.save(payment);

       Transaction transaction = new Transaction();
       transaction.setAmount(paymentProcessDto.getAmount());
       transaction.setPayment(payment);
       transaction.setTimestamp(LocalDateTime.now());
       transaction.setType(Transaction.Type.PAY);
       transaction.setStatus(Transaction.Status.CREATED);

       transactionRepository.save(transaction);

        log.info("In PaymentProcessService create new Transaction with Type.PAY and Status.CREATED Time: {}, Amount: {}",
                payment.getTimestamp(), payment.getAmount());

       return new PaymentProcessResponseDto(PaymentCondition.CREATED);
    }

    @Override
    @Transactional
    public PaymentProcessResponseDto cancel(PaymentProcessDto paymentProcessDto) {
        Transaction transaction = transactionRepository.findTransactionByIdAndStatus(paymentProcessDto.getTransactionId(),
                Transaction.Status.FINISHED).orElse(null);

        if(transaction == null || transaction.getType() != Transaction.Type.PAY){
            return new PaymentProcessResponseDto(PaymentCondition.ERROR);
        }

        Payment payment = transaction.getPayment();

        if(payment.getTransactionList().stream().anyMatch(p -> p.getType() != Transaction.Type.PAY)){
            return new PaymentProcessResponseDto(PaymentCondition.ERROR);
        }
        Transaction currentTransaction = new Transaction();
        currentTransaction.setAmount(paymentProcessDto.getAmount());
        currentTransaction.setPayment(payment);
        currentTransaction.setTimestamp(LocalDateTime.now());
        currentTransaction.setType(Transaction.Type.CANCEL);
        currentTransaction.setStatus(Transaction.Status.CREATED);

        transactionRepository.save(currentTransaction);

        log.info("In PaymentProcessService create new Transaction with Type.CANCEL and Status.CREATED Time: {}, Amount: {}",
                currentTransaction.getTimestamp(), currentTransaction.getPayment());

        return new PaymentProcessResponseDto(PaymentCondition.CREATED);
    }
}
