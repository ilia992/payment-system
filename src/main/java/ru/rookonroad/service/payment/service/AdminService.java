package ru.rookonroad.service.payment.service;

import org.springframework.http.ResponseEntity;
import ru.rookonroad.service.payment.dto.AuthenticationRequestDto;
import ru.rookonroad.service.payment.dto.UsersRequestDto;
import ru.rookonroad.service.payment.dto.UsersResponseDto;
import ru.rookonroad.service.payment.entity.Users;
import java.util.List;
import java.util.UUID;

public interface AdminService {
    UsersResponseDto getUserById(UUID id);
    List<UsersResponseDto> getAllUsers(Users.Role role);
    UsersResponseDto deleteUserById(UUID id);
    UsersResponseDto registerUser( UsersRequestDto usersRequestDto);
    ResponseEntity login(AuthenticationRequestDto requestDto);
}
