package ru.rookonroad.service.payment.service;
import ru.rookonroad.service.payment.dto.SystemRequestDto;
import ru.rookonroad.service.payment.dto.SystemResponseDto;
import java.util.List;

public interface SystemService {
    SystemResponseDto getById(Long id);

    SystemResponseDto create(SystemRequestDto systemRequestDto);

    SystemResponseDto update(Long id, SystemRequestDto update);

    SystemResponseDto delete(Long id);

    List<SystemResponseDto> getAll();
}
