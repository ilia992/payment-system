package ru.rookonroad.service.payment.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.rookonroad.service.payment.service.NotificationService;

@Service
@Slf4j
@RequiredArgsConstructor
public class NotificationServiceImpl implements NotificationService {
    @Override
    public void sendSuccessfulTransaction() {
        log.info("In NotificationService transaction was successful");
        System.out.println("Transaction was successful");
    }

    @Override
    public void sendFailedTransaction() {
        log.info("In NotificationService transaction was failed!!!");
        System.out.println("Transaction was failed!!!");
    }
}
