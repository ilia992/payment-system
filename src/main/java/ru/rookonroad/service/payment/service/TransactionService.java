package ru.rookonroad.service.payment.service;

import ru.rookonroad.service.payment.dto.PaymentResponseDto;
import ru.rookonroad.service.payment.dto.SystemRequestDto;
import ru.rookonroad.service.payment.dto.SystemResponseDto;
import ru.rookonroad.service.payment.dto.TransactionResponseDto;

import java.util.List;

public interface TransactionService {
    TransactionResponseDto getById(Long id);

    TransactionResponseDto delete(Long id);

    List<TransactionResponseDto> getAll();
}
