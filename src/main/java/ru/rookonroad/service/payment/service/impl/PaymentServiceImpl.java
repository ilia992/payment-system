package ru.rookonroad.service.payment.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.rookonroad.service.payment.dto.PaymentResponseDto;
import ru.rookonroad.service.payment.entity.Payment;
import ru.rookonroad.service.payment.mapper.PaymentMapper;
import ru.rookonroad.service.payment.repository.PaymentRepository;
import ru.rookonroad.service.payment.service.PaymentService;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class PaymentServiceImpl implements PaymentService {

    private final PaymentRepository paymentRepository;
    private final PaymentMapper paymentMapper;

    @Override
    public PaymentResponseDto getById(Long id) {
        Payment payment = paymentRepository.findById(id).orElse(null);
        if(payment == null){
            log.warn("IN getById (payment) - Payment with id: {} not found", id);
        }
        log.info("IN getById (payment) - the Payment with id {} was sent", id);
        return paymentMapper.toDto(payment);
    }

    @Override
    public PaymentResponseDto delete(Long id) {
        Payment payment = paymentRepository.findById(id).orElse(null);
        if(payment == null){
            log.warn("IN delete(payment) - Payment with id: {} not found", id);
        }
        if (payment != null) {
            paymentRepository.delete(payment);
            log.info("IN delete(payment) - Payment with id {} was deleted", id);
        }
        return paymentMapper.toDto(payment);
    }

    @Override
    public List<PaymentResponseDto> getAll() {
        List<Payment> list = paymentRepository.findAll();
        if(list.isEmpty()){
            log.warn("IN getAll(payment) - List of Payments is Empty");
        }
        log.info("IN getAll(payment) - all Payments was sent to Dto");
        return list.stream().map(paymentMapper::toDto).collect(Collectors.toList());
    }

}
