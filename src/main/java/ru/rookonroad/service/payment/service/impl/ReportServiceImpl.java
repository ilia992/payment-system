package ru.rookonroad.service.payment.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.rookonroad.service.payment.dto.PaymentReportDto;
import ru.rookonroad.service.payment.dto.PaymentResponseDto;
import ru.rookonroad.service.payment.dto.TransactionReportDto;
import ru.rookonroad.service.payment.dto.TransactionResponseDto;
import ru.rookonroad.service.payment.mapper.PaymentMapper;
import ru.rookonroad.service.payment.mapper.TransactionMapper;
import ru.rookonroad.service.payment.repository.PaymentRepository;
import ru.rookonroad.service.payment.repository.TransactionRepository;
import ru.rookonroad.service.payment.service.ReportService;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;


@Service
@Slf4j
@RequiredArgsConstructor
public class ReportServiceImpl implements ReportService {

    private final PaymentRepository paymentRepository;
    private final TransactionRepository transactionRepository;
    private final PaymentMapper paymentMapper;
    private final TransactionMapper transactionMapper;

    @Override
    public PaymentReportDto getPaymentAfterTime(LocalDateTime timestamp) {
        List<PaymentResponseDto>paymentsAfterTime = paymentRepository.findPaymentByTimestampAfter(timestamp)
                .stream()
                .map(paymentMapper::toDto)
                .collect(Collectors.toList());
        if(paymentsAfterTime.isEmpty()){
            log.warn("IN getPaymentAfterTime - Payments {} found by Timestamp {}", null, timestamp);
        }
        PaymentReportDto paymentReportDto = new PaymentReportDto();
        paymentReportDto.setPaymentList(paymentsAfterTime);
        log.info("IN getPaymentAfterTime send paymentReportDto");
        return paymentReportDto;
    }

    @Override
    public PaymentReportDto getPaymentBetweenTime(LocalDateTime timeStart, LocalDateTime timeEnd) {
        List<PaymentResponseDto> paymentResponseDtoList = paymentRepository.findPaymentByTimestampBetween(timeStart, timeEnd)
                .stream()
                .map(paymentMapper::toDto)
                .collect(Collectors.toList());
        if(paymentResponseDtoList.isEmpty()){
            log.warn("IN getPaymentBetweenTime - Payments {} found between Timestamp {} and {}", null, timeStart, timeEnd);
        }
        PaymentReportDto paymentReportDto = new PaymentReportDto();
        paymentReportDto.setPaymentList(paymentResponseDtoList);
        log.info("IN getPaymentBetweenTime send paymentReportDto");
        return paymentReportDto;
    }

    @Override
    public TransactionReportDto getTransactionAfterTime(LocalDateTime timestamp) {
        List<TransactionResponseDto> paymentResponseDtoList = transactionRepository.findTransactionByTimestampAfter(timestamp)
                .stream()
                .map(transactionMapper::toDto)
                .collect(Collectors.toList());
        if(paymentResponseDtoList.isEmpty()){
            log.warn("IN getTransactionAfterTime - Transactions {} found after Timestamp {}", null, timestamp);
        }
        TransactionReportDto transactionReportDto = new TransactionReportDto();
        transactionReportDto.setTransactionList(paymentResponseDtoList);
        log.info("IN getTransactionAfterTime send transactionReportDto");
        return transactionReportDto;
    }

    @Override
    public TransactionReportDto getTransactionBetweenTime(LocalDateTime timeStart, LocalDateTime timeEnd) {
        List<TransactionResponseDto>transactionResponseDtoList = transactionRepository.findTransactionByTimestampBetween(timeStart, timeEnd)
                .stream()
                .map(transactionMapper::toDto)
                .collect(Collectors.toList());
        if(transactionResponseDtoList.isEmpty()){
            log.warn("IN getTransactionBetweenTime - Transactions {} found between Timestamp {} and {}", null, timeStart, timeEnd);
        }
        TransactionReportDto transactionReportDto = new TransactionReportDto();
        transactionReportDto.setTransactionList(transactionResponseDtoList);
        log.info("IN getTransactionBetweenTime send transactionReportDto");
        return transactionReportDto;
    }

}
