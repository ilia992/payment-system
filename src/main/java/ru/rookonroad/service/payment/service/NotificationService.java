package ru.rookonroad.service.payment.service;

public interface NotificationService {
    void sendSuccessfulTransaction();
    void sendFailedTransaction();

}
