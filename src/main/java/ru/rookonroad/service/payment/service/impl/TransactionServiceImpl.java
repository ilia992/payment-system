package ru.rookonroad.service.payment.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.rookonroad.service.payment.dto.TransactionResponseDto;
import ru.rookonroad.service.payment.entity.Transaction;
import ru.rookonroad.service.payment.mapper.TransactionMapper;
import ru.rookonroad.service.payment.repository.TransactionRepository;
import ru.rookonroad.service.payment.service.TransactionService;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {
    private final TransactionRepository transactionRepository;
    private final TransactionMapper transactionMapper;

    @Override
    public TransactionResponseDto getById(Long id) {
        Transaction transaction = transactionRepository.findById(id).orElse(null);
        if(transaction == null){
            log.warn("IN getById (transaction) - Transaction with id: {} not found", id);
        }
        log.info("IN getById (transaction) - the Transaction with id {} was sent to Dto", id);
        return transactionMapper.toDto(transaction);
    }

    @Override
    public TransactionResponseDto delete(Long id) {
        Transaction transaction = transactionRepository.findById(id).orElse(null);
        if(transaction == null){
            log.warn("IN getById (transaction) - Transaction with id: {} not found", id);
        }
        if (transaction != null) {
            transactionRepository.delete(transaction);
        }
        log.info("IN delete (transaction) - the Transaction with id {} deleted", id);
        return transactionMapper.toDto(transaction);
    }

    @Override
    public List<TransactionResponseDto> getAll() {
        List<Transaction> list = transactionRepository.findAll();
        if(list.isEmpty()){
            log.warn("IN getAll(transaction) - List of Transactions is Empty");
        }
        log.info("IN getAll(transaction) - all Transactions was sent to Dto");
        return list.stream().map(transactionMapper::toDto).collect(Collectors.toList());
    }
}