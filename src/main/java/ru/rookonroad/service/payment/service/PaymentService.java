package ru.rookonroad.service.payment.service;

import ru.rookonroad.service.payment.dto.PaymentResponseDto;
import ru.rookonroad.service.payment.dto.SystemRequestDto;
import ru.rookonroad.service.payment.dto.SystemResponseDto;

import java.util.List;

public interface PaymentService {
    PaymentResponseDto getById(Long id);

    PaymentResponseDto delete(Long id);

    List<PaymentResponseDto> getAll();
}
