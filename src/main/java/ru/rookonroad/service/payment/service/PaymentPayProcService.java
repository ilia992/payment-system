package ru.rookonroad.service.payment.service;

import org.springframework.stereotype.Service;
import ru.rookonroad.service.payment.component.PaymentProcessComponent;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class PaymentPayProcService {
    public PaymentPayProcService(PaymentProcessComponent paymentProcessComponent) {
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        executorService.submit(() -> {
            paymentProcessComponent.process();
            paymentProcessComponent.payment();
        });
    }
}
