package ru.rookonroad.service.payment.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.rookonroad.service.payment.dto.AuthenticationRequestDto;
import ru.rookonroad.service.payment.dto.UsersRequestDto;
import ru.rookonroad.service.payment.dto.UsersResponseDto;
import ru.rookonroad.service.payment.entity.Users;
import ru.rookonroad.service.payment.mapper.UsersMapper;
import ru.rookonroad.service.payment.repository.UsersRepository;
import ru.rookonroad.service.payment.security.JwtTokenProvider;
import ru.rookonroad.service.payment.service.AdminService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Slf4j
@Service
public class AdminServiceImpl implements AdminService {
    private final UsersMapper usersMapper;
    private final UsersRepository usersRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;

    @Override
    public UsersResponseDto getUserById(UUID id) {
        Users user = usersRepository.findById(id).orElse(null);
        if(user == null){
            log.warn("IN getUserById - User: {} found by id: {}", null, id );
        }
        log.info("IN getUserById user with id {} was sent", id);
        return usersMapper.toDto(user);
    }

    @Override
    public ResponseEntity login(AuthenticationRequestDto requestDto) {
        String username = requestDto.getUsername();
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, requestDto.getPassword()));
        Users user = usersRepository.findByUserName(username).orElse(null);

        if(user == null){
            throw new UsernameNotFoundException("User with username: " + username + " not found");
        }
        String token = jwtTokenProvider.createToken(user.getId().toString(), user.getRole());

        Map<Object, Object> response = new HashMap<>();
        response.put("username", username);
        response.put("token", token);
        log.info("IN login created username - {} and token", username);
        return ResponseEntity.ok(response);
    }

    @Override
    public List<UsersResponseDto> getAllUsers(Users.Role role) {
        List<Users> list = usersRepository.findAllByRole(role);
        log.info("IN getAllByRole - {} users found", list.size());
        return list.stream().map(usersMapper::toDto).collect(Collectors.toList());
    }
    @Override
    public UsersResponseDto deleteUserById(UUID id) {
        Users user = usersRepository.findById(id).orElse(null);
        if(user == null){
            log.warn("IN deleteUserById - User: {} found by id: {}", null, id );
        }
        assert user != null;
        usersRepository.delete(user);
        log.info("IN deleteUserById - User was deleted");
        return usersMapper.toDto(user);
    }
    @Override
    public UsersResponseDto registerUser(UsersRequestDto usersRequestDto) {
        Users user = usersMapper.toEntity(usersRequestDto);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        usersRepository.save(user);
        log.info("IN register - user: successfully registered");
        return usersMapper.toDto(user);
    }
}
