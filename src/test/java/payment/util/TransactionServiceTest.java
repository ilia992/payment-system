package payment.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.rookonroad.service.payment.dto.TransactionResponseDto;
import ru.rookonroad.service.payment.entity.Transaction;
import ru.rookonroad.service.payment.mapper.TransactionMapper;
import ru.rookonroad.service.payment.repository.TransactionRepository;
import ru.rookonroad.service.payment.service.TransactionService;
import ru.rookonroad.service.payment.service.impl.TransactionServiceImpl;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class TransactionServiceTest {

    private final TransactionRepository transactionRepository;
    private final TransactionMapper transactionMapper;
    private final TransactionService transactionService;

    public TransactionServiceTest() {
        this.transactionRepository = mock(TransactionRepository.class);
        this.transactionMapper = mock(TransactionMapper.class);
        this.transactionService = new TransactionServiceImpl(transactionRepository, transactionMapper);
    }

    @Test
    public void getByIdShouldGetEntity(){
        Long testId = 1L;
        Transaction transaction = new Transaction();
        transaction.setId(testId);
        TransactionResponseDto dto = new TransactionResponseDto();
        dto.setId(testId);
        when(transactionRepository.findById(testId)).thenReturn(Optional.of(transaction));
        when(transactionMapper.toDto(transaction)).thenReturn(dto);
        TransactionResponseDto resultDto = transactionService.getById(testId);
        Assertions.assertEquals(dto, resultDto);
    }
    @Test
    public void getAllByIdShouldGetAllEntity(){
        List<Transaction>transactionList = new ArrayList<>();
        Transaction t1 = new Transaction();
        t1.setId(1L);
        Transaction t2 = new Transaction();
        t2.setId(2L);
        Transaction t3 = new Transaction();
        t3.setId(3L);
        transactionList.add(t1);
        transactionList.add(t2);
        transactionList.add(t3);

        when(transactionRepository.findAll()).thenReturn(transactionList);
    }
}
