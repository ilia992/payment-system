# Payment system

> ### Spring boot codebase containing real examples (CRUD, security, multithreading, patterns, etc) that adheres to the [Payment system](https://gitlab.com/ilia992/payment-system.git). 
 It's a simple analog of real pay system. Many users will have a personal account, 
 where they can make transactions to different users. 

# Data base
In this project used PostgresQl 14. The schema of tables: ![](src/main/resources/img/db_diagram_payment-system.png)

# How it works
* First, it's authentication and authorisation with JWT token.
* every Person in the service have the role (admin, employee, client).
* The Person have the own system(account).
* In account, you can make payment operations.
* Every payment operation may can one or more transactions.
* Its transactional operations and we checked of type operation(pay, cancel).
* The program check the type in other thread. 
* We change the status of transactions depending on the type.
* Every transaction have status(created, processing, finished, failed).

# Security

Integration with Spring Security and add other filter for jwt token process.

The secret key is stored in `application-dev.yml`.

# Getting started

You'll need Java 8 installed.

To test that it works, open a browser tab at http://localhost:8080/tags .  
Alternatively, you can run

    curl http://localhost:8080/swagger-ui/index.html

